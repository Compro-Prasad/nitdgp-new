import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pageTitle: "Test Title",
    tabs: [],
    currentTab: ""
  },
  getters: {
    tabs: state => state.tabs,
    currentTab: state => state.currentTab
  },
  mutations: {
    setPageTitle(state: any, newTitle: string) {
      state.pageTitle = newTitle;
    },
    setTabs(state: any, newTabs: Array<string>) {
      state.tabs = newTabs;
    },
    setCurrentTab(state: any, payload: any) {
      if (!payload.$route.params.hasOwnProperty("tab")) {
        let url = payload.$route.path;
        if (!url.endsWith("/")) url += "/";
        url += payload.tab;
        payload.$router.push(url);
      } else if (payload.$route.params.tab !== payload.tab) {
        payload.$router.push({ params: { tab: payload.tab } });
      }
      state.currentTab = payload.tab;
    }
  },
  actions: {},
  modules: {}
});
