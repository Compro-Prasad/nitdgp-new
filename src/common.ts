const backend_url = "https://admin.nitdgp.ac.in";

function check_and_get(obj: any, path: string) {
  return path.split(".").reduce((obj, key) => {
    if (key in obj) {
      return obj[key];
    }
  }, obj);
}

function generate_url(path: string) {
  if (path.startsWith("/")) {
    if (path.endsWith("?format=json")) {
      return backend_url + path;
    } else if (path.endsWith("/")) {
      return backend_url + path + "?format=json";
    } else {
      return backend_url + path + "/?format=json";
    }
  }
  return backend_url;
}

function clean_object(
  obj: any,
  blackList: string[] = [],
  whiteList: string[] = []
) {
  if (!obj || obj.constructor !== Object) return;
  for (let tab in obj) {
    let value = obj[tab];
    if (whiteList.length !== 0 && !whiteList.includes(tab)) {
      delete obj[tab];
    } else if (typeof value !== "number") {
      if (typeof value === "object" && Object.keys(value).length === 0) {
        delete obj[tab];
      } else if (value.length === 0) {
        delete obj[tab];
      }
    }
  }
  blackList.forEach(tab => {
    if (obj.hasOwnProperty(tab)) {
      delete obj[tab];
    }
  });
}

export { check_and_get, generate_url, clean_object };
